---

title: CYK Parsing
layout: default

---

{% capture rules %}{% include_relative rules.md %}{% endcapture %}
{{rules  | markdownify}}

|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|           |       | *PP*  |       |           |       |   |
| V', VP, NP |      |       | _NP_  |           |       | NP |
| N, V      | NP    | _P_   | D     | N         | P     | N |
|tell       | me    | about | my    | meetings  | at    | 3 |


<a href="{{site.baseurl}}/CYKParse/cyk21.html">Next</a>
