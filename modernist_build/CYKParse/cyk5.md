---

title: CYK Parsing
layout: default

---

{% capture rules %}{% include_relative rules.md %}{% endcapture %}
{{rules  | markdownify}}

|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
| N, V | NP | P  | D |   |   |   |
|tell | me | about | my | meetings | at | 3 |


<a href="{{site.baseurl}}/CYKParse/cyk6.html">Next</a>
