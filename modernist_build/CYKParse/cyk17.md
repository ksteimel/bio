---

title: CYK Parsing
layout: default

---

{% capture rules %}{% include_relative rules.md %}{% endcapture %}
{{rules  | markdownify}}

|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
| V', VP, NP |       |       | NP    |           |       | *NP* |
| N, V      | NP    | P     | D     | N         | P     | _N_ |
|tell       | me    | about | my    | meetings  | at    | 3 |


<a href="{{site.baseurl}}/CYKParse/cyk18.html">Next</a>
