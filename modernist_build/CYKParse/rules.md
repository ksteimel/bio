### Rules
---
| S => NP VP | S => VP | VP => V NP |
| V' => V NP | VP => V' PP | PP => P NP |
| NP => D N | NP => N | NP => AdjP NP |
| NP => D N' | N' => N PP | NP => N PP |
| V' => V' PP |  NP => me | P => about |
| P => at |  N => 3 |  N => tell | 
| N => meetings | V => tell | D => my | 
