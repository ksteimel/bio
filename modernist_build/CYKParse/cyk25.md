---

title: CYK Parsing
layout: default

---

{% capture rules %}{% include_relative rules.md %}{% endcapture %}
{{rules  | markdownify}}

|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|           |       |       |       |           |       |   |
|  _V'_       |       |       |       |           |       |   |
|           |       | **PP**    |     |           | PP    |   |
| **V'**, VP, NP |      |       | NP    |           |       | NP |
| N, V      | NP    | P     | D     | N         | P     | N |
|tell       | me    | about | my    | meetings  | at    | 3 |


<a href="{{site.baseurl}}/CYKParse/cyk26.html">Next</a>
