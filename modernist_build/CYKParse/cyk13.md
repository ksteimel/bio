---

title: CYK Parsing
layout: default

---

{% capture rules %}{% include_relative rules.md %}{% endcapture %}
{{rules  | markdownify}}

|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
| V', VP, NP | $$ |   |   |   |   |   |
| N, V | NP | P  | D | N | P | N |
|tell | me | about | my | meetings | at | 3 |


<a href="{{site.baseurl}}/CYKParse/cyk14.html">Next</a>
