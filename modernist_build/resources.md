---

layout: default
title: Resources

---

This page just includes some resources I'd like to make available. 

# Resources for Indiana University Students

## Indiana University LaTeX template for presentations

[This template](assets/documents/IU-presentation-tex.zip) was largely taken from Vít Novotný's template for Masaryk University with some modifications to the color schemes and the addition of the IU sigil. 

I am by no means a designer though so if you see something in there that you don't like, feel free to email me or message me on mastodon with revision suggestions. 

## Super-computer overview at IU

[This presentation](assets/documents/Super_Computer_Access.pdf) provides an overview of the different types of super computer access available to students at IU. All of these computers may not be accessible to you. The audience of this presentation was Linguistics graduate students at Indiana University. Some of these servers are granted automatically to graduate students but are not granted to undergraduates.

## Conference Presentations

### [LREC Impact Workshop](assets/documents/lrec-impact.pdf)
