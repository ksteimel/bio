---

title: Completed Switch to Nginx
layout: default
category: infrastructure 

---

I now have all my services set-up using ssl and nginx reverse proxying. These are the services I'm currently running:

- This blog
- Personal Ampache server
- Nextcloud instance
- gitea instance
- r-shiny server
- Sharelatex server
- Mattermost chat server

All of these were set up without using wildcard certificates. Part of my problem was in thinking that my dynamic dns address (duckdns.org) would not work with subdomains. I spent a good while beating my head against a wall, trying to come up with a way for my prettier domain name (steimel.info) to have the subdomains. This ended up causing issues because the challenge method for ssl with letsencrypt was not hitting the CNAME record I set up with godaddy. 

By switching to simply use the longer urls from my dynamic dns provider, I was able to establish everything as subdomains and do ssl challenges using certbot. 

I do, however, want to look into using my cname record in godaddy to do the redirect as the urls it generates are much more concise. 


