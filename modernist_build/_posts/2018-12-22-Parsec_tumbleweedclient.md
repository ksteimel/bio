---
title: Gaming on OpenSuse tumbleweed using parsec
category: infrastructure
layout: default
 
---

[Parsec](https://parsecgaming.com) is a piece of software that allows you to 
use a remote client, like a smart phone, android tv, or laptop, to game on
a beefy host machine with a gpu. This is a brief description of how
I was able to get the parsec client working on OpenSuse Tumbleweed. 

## Minor points

- There is no linux hosting using parsec. The host computer must be a windows computer. That is why this guide only explains how to set up the client in tumbleweed.
- Follow these instructions at your own risk. These are what worked for me but they may not be universal.

### Install rpm-build and alien


```bash
wget http://download.opensuse.org/repositories/utilities/openSUSE_Factory/x86_64/alien-8.88-3.29.x86_64.rpm
sudo zypper install alien-8.88-3.29.x86_64.rpm
sudo zypper install rpm-build
```

Or, to be more secure, visit the one click install page for the alien package at [https://software.opensuse.org/package/alien\](https://software.opensuse.org/package/alien), click "Show Experimental Packages", and click the *1 Click Install* button under the utilities package.

You may get a question regarding what to do about the lack of GPG keys if you do not do the 1 Click Install method. Type `i` in the terminal when prompted to ignore the missing gpg key.


### Install parsec

```
wget https://s3.amazonaws.com/parsec-build/package/parsec-linux.deb
sudo alien -r parsec-linux.deb
# This produces a file named parsec-147-10.x86_64.rpm
sudo zypper install parsec-147-10.x86_64.rpm
```

### Launching parsec

Create an account on [parsec.com](parsec.com).

Start parsec on your host computer, or launch a cloud instance.

You should then be able to start parsec from the terminal by running `parsecd`, or by finding it in the launcher. You will get a prompt for your email and password for the account you created. If everything goes well, you'll have a desktop window open up.


