---

title: Roasting Indonesian RFA Balinese Kintamani
layout: default
category: coffee

---

I have roasted about 2 pounds of [Indonesian Bali Kintamani](https://www.burmancoffee.com/coffeelist/new-popups/indonesianbali.html)
over the past month or so.

This blog post is just a summary of my impressions of how well I can get this coffee to roast on my Freshroast air roaster.
The page I linked to above provides good information about the flavor of the coffee. However, I hardly ever see reviews of
the aspects of coffee that make it better suited to an air roaster. I imagine this would be too specific for a green coffee retailer to 
focus on this aspect. However, I imagine a large amount of the people reading up on the bean notes are amateur roasters like myself.
I suspect that professional roasters would be more familiar with what coffees from this region and with this process are like. 
I could be quite wrong though since I'm not a professional roaster by any means. 
![Bali Kintamani]({{site.baseurl}}/assets/images/Kintamani.jpg)

## Some quick notes about this coffee from the seller (Burman): 

- Dry processed on raised beds
- Shade grown under Erythrina, Tangerine, and Orange
- Grown in Bali

# Air roaster specific points

Overall, I would give this coffee an 8 or 9 out of 10 when roasted on the Freshroast. It's not hard at all to get a decent roast
at a variety of different points using this bean. The beans are large and have a good flat side. This seems to give the beans more surface area for the air currents from the Freshroast to push on. The result is good circulation at all stages of the roast. I still do a brief dry stage at low heat before the actual roast begins. During this stage they show good, even yellowing and begin to turn brown. They circulate very well even at this, their most dense stage. They maintain a consistent roast throughout with first crack coming fairly quickly (2.5-3 minutes after the dry stage is complete and I move to high heat).

# Flavor 

These beans produce a nice fruity flavor. Unlike most of the other jammy beans I've had, however, these are not high in acid content. This makes them much more drinkable than other jammy coffees. I tend to enjoy this coffee on the lighter side, halting the roast about 2 minutes after first crack has started.

I need to try a couple more darker roast points before I feel comfortable talking about the differences between different roasts. 

The reduced acidty produces a more pleasant mouthfeel for me as well. 

### This is a coffee that I want to drink more of. 
