---

title: Roasting Honduran SHG EP Santa Rosa
category: coffee
layout: default

---

I bought 5 pounds of [Honduran SHG EP Santa Rosa](http://www.burmancoffee.com/blog/honduran-shg-ep-santa-rosa-copan/) from Burman coffee traders about a month and a half ago. I've now roasted about 3 pounds of this. While this coffee is very good. It is not ideal for air roasting due to the small size of the beans and the large amount of chaff produced. This blog post is a summary of my impressions of the coffee with an emphasis on the technique that I use for roasting it effectively in an air roaster.

However, I have come up with a method that I use to roast this coffee evenly and reduce the amount of chaff that makes it into your coffee filter. Having chaff left over can give the coffee a woody taste. Literally tastes like sawdust if there's too much. 

## Basic notes
- Grown by community co-ops in Santa Rosa de Copan
- I believe this is dry processed (due to the amount of chaff coming off of the beans). However, there is no information provided on Burman's website anymore. 

## Roasting process
If I use the method discussed in my [roast notes]({{site.baseurl}}/coffee/2018/08/11/RoastingProcess.html), the beans are very unevenly roasted and have a large amount of chaff still stuck to the beans. 

To get a more even roast and remove more chaff, I do two things. I extend the drying stage by another 45 seconds and I use a much shorter range on the fan speed control. 

For the fan speed control, I start out at max speed like normal. However, the lowest I go is about 1 o'clock on the dial. These beans are difficult to circulate and allowing them to lose more moisture during drying helps with this significantly. Keeping fan speeds higher both helps the beans roast more evenly and helps to dislodge more of the chaff from the beans. 

In addition, I typically hold onto the high temperature setting for longer than I ordinarily would. I do this because I was getting a very drawn out and uneven first crack. This is probably because the higher fan speeds were increasing the effective temperature. 

As an additional step to remove chaff, I blow on the beans after griding and just before roasting. I do this over a trash can and I shake the coffee grinds a little and then blow on them again. This helps to remove a lot more of the remaining chaff if my previous methods were unsuccessful. 

I don't think I will be buying this bean again while I have an air roaster.  
