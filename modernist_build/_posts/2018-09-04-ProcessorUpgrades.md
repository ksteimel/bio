---

title: I upgraded my processors to dual e5-2680's
layout: default

---

I have upgraded the processors in my Quanta windmill to dual e5-2680's. This has resulted in a rather large increase in linpack scores (pushing 306 GFLOPS now, while I was originally around 275). 

However, unintended side effect is that the linpack benchmark is so intense that it causes the system to become unstable. I believe this is due to the amount of power being drawn to support these processors as my Kill-A-Watt indicates that over 350 watts are being drawn by just the one node. I know that the power supply for both nodes can support 750 watts but I had initially thought that if one node was powered off, this would allow the other to consume the full 750 watts. However, this does not appear to be the case, the backplane appears to have a power limit for each node. 

Turning off turbo boost on linpack fixes this issue. However, the algorithms that I'm running are not as computationally taxing as dense systems of equations (the data I'm working with is almost always sparse). I believe this reduces the amount of power consumption, allowing me to use turbo boost for everything but linpack benchmarking. 
