---

title: Sterling Engine
layout: default

---

I recently moved and I finally found the glass pieces to my Sterling engine again. This little contraption is an engine that generates kinetic energy from any heat source. Here's a video of this thing going.

<video width="400" height="300" controls>
  <source src="https://ksteimel.duckdns.org/assets/images/SterlingEngine.webm" type="video/webm">
  Your browser does not support webm style videos (e.g. you have internet exporer or safari)
</video>

The flame is just a glass lamp. Right now it's burning isopropol alcohol but it can burn high proof ethanol as well. I just didn't have any on hand to try this with. 

The flame heats up a glass bulb (this is the part I lost for a while). The air inside this bulb heats up. When this happens it moves the piston inside the bulb backwards. While the piston is moving, there is also air escaping past the piston inbetween the piston and the glass bulb. As the piston moves back, the air is forced through a hole into another chamber. 

Inside this chamber, there is another piston that is linked to the first with a solid bar. When the first piston (the one you can see in the video), moves back, the other one also moves back, helping to suck air into this back chamber. The back chamber functions in a similar way to the front chamber except instead of having a heat source, the fins on the outside of the chamber help to cool the air that makes it past the second piston. While the air in the glass bulb is expanding the air in the metal chamber is contracting, drawing the pair of pistons back. The momentum from the flywheel then brings the pistons forward and the process starts all over again.

This is a closed system. There is no exhaust (except for the gases coming off of the heat source). In the future, I hope to see if I can get this to turn over using heat from the sun. I imagine I would need a black glass bulb and a magnifying glass to accomplish this. 
