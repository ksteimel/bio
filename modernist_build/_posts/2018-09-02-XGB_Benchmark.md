---

title: XGboost benchmarks
layout: default
category: infrastructure

---

Running the benchmark scripts from xgboost with different tree settings on my new system with dual e5-2680's and a 1050ti gives interesting results. 

exact: 385.617
gpu\_exact: 365.375

When you compare this to the benchmark scores for a i7-6700k and a titan X on [the documentation page](https://xgboost.readthedocs.io/en/latest/gpu/index.html), you can see that my system has a much stronger cpu score and a weaker gpu score to the point where they are almost equivalent.
