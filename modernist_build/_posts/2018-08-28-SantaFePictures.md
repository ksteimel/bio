---

layout: default
title: Pictures from COLING 2018 in Santa Fe, NM

---

[These](https://cloud.ksteimel.duckdns.org/index.php/s/MfcFqY9rQ6Ydzme) are some pictures from my trip to Santa Fe, New Mexico for COLING2018. 

Most of these were taken at Bandalier National Monument to the North while others were taken around town in Santa Fe. Not all of them are great but there's some good pictures in there.


