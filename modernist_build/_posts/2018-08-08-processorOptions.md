---

title: Processor upgrades for Quanta Windmill
layout: default
category: infrastructure

---

I've just installed a 1050ti in one of the nodes of my Quanta windmill.
I was able to get 59 fps at 1080p on the Tomb Raider benchmarks and 90+ fps at 720p.
I will write up more on the performance of this card once I get cuda installed and running on it. 
I was quite surprised that a barebones server like the quanta windmill could handle a 1050ti. Even though the 1050ti has no external power requirements and draws everything it needs from the pcie, it still pulls 75w which was more than I expected the windmill  to handle. 

Now that I know this works, I don't need to spend money on a new computer to support my 1050ti. 
I am considering spending this money on a faster set of processors to put in this machine. 
To determine if the upgrade is worth it, I'm compiling some stats about the processors to help me decide.


# The processors: 
I'm looking at upgrading from dual e5-2650's to e5-2680 or e5-2680v2. I believe this quanta board will support e5v2 chips per [this forum](https://forums.servethehome.com/index.php?threads/potential-deal-2-x-dual-2011-nodes-199-quanta-openrack.6856/).
However, there is still a level of uncertainty there. 

## E5-2650 (current processors):
- 8 core, 16 thread. 
- Base frequency of 2 GHz
- I've observed all core turbos of 2.5 GHz during hpl benchmarking

## E5-2680:
- 8 core, 16 thread
- Base frequency of 2.7 GHz
- All core turbo reported: [3.1 GHz](http://www.cpu-world.com/CPUs/Xeon/Intel-Xeon%20E5-2680.html)

## E5-2680v2:
- 10 core, 20 thread
- Base frequency of 2.8 GHz
- All core turbo reported: [3.1 GHz](http://www.cpu-world.com/CPUs/Xeon/Intel-Xeon%20E5-2680%20v2.html)

# Theoretical Peak floating point performance

## e5-2650 (current): 

Single precision: 640
- (8x2x2.5x16)
- All core turbo observed during linpack is 2.5 GHz
- [16 instructions per cycle](https://stackoverflow.com/questions/15655835/flops-per-cycle-for-sandy-bridge-and-haswell-sse2-avx-avx2)

Double precision: 320
- 8 instructions per cycle 

Observed double precision: 260.5787 GFLOPS

## e5-2680
e5-2680: theoretical single precision = 8 x 2 x 3.1 x 16 = 793.6 GFLOPS

double precision = 8 x 2 x 3.1 x 8 = 396.8 GFLOPS

## e5-2680v2: 10x2x3.1x16 = 992

10x2x3.1x8 = 496
