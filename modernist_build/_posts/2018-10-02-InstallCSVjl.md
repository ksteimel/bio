---

title: How to install CSV.jl on Fedora
layout: default

---

I attempted to install CSV.jl on Fedora with julia 0.7. However, there was an error in building `CodecZlib.jl`. The error message I got was 

```
│ ERROR: LoadError: LoadError: could not load library "libLLVM"
│ libLLVM.so: cannot open shared object file: No such file or directory
│ Stacktrace:
```

If you get this error, you can resolve it by installing llvm-devel. 

```
sudo dnf install llvm-devel
```
