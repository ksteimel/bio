---

title: Introduction presentation to Computational Linguistics
category: cl
layout: default

---

[This]({{site.baseurl}}/assets/documents/Computational_Linguistics.pdf) is a presentation introducing what Computational Linguistics is at a high level. 

Keep in mind that this is for an undergraduate Introduction to Linguistics course. Some things are simplified and some things are not mentioned at all for the sake of time. The intent was to be engaging not necessarily to be thorough. 
