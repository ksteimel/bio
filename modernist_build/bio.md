---

layout: default

---

Hey there! My name is Kenneth Steimel and I am a Computational Linguistics PhD student at Indiana University Bloomington.
I am originally from St. Charles, Missouri, a suburb of St. Louis.

![St.Louis]({{site.baseurl}}assets/images/StLouisFlag.png){: .center-image width="350px"}

I live with my Fiancé, Lindsay Logan, and our cat, Phoebe, in Bloomington Indiana.

![Phoebe]({{site.basurl}}assets/images/Phoebe.jpg){: .center-image width="300px"}

--- 

# Research
 
I have quite a few research interests including morphological analysis, sentiment analysis, and part of speech tagging. 
My primary research question that I investigate is whether methods that apply to Indo-European languages can extend to non-Indo-European languages. The non-Indo-European languages that I work with in this regard are Bantu languages like Swahili and the Luyia languages.

I have experience doing language documentation: as an undergraduate at the University of Missouri I
conducted research on several [Luyia languages](https://www.ethnologue.com/language/luy) including Tiriki, Bukusu, and Wanga. [My honors thesis]({{site.baseurl}}/assets/documents/WangaRevised.pdf) as an undergraduate was a description (with brief analysis) of the noun tone system of Wanga. 

![Personal Picture](assets/images/Pic.jpg "My mom and I."){: .center-image width="400px"}

---

# Employment

I work as an assistant instructor for the Linguistics Department at Indiana Univeristy. I am teching L103-Introduction to Language Fall 2018 and Spring 2019. 

Last year, I worked as a student editor at the [LINGUIST List](https://new.linguistlist.org). This organization is the central source for information in the linguistics community. We have information about job postings, conferences in the field, books relevant to linguistics and much more. I edit conferences and Queries in addition to several smaller areas.

I also moderate the [Ask-A-Linguist](askaling.linguistlist.org) service provided by the LINGUIST List. If you have a question related to language or linguistics, create an account and ask!

---

# Hobbies

I roast coffee nearly every day and I love cooking. I think I'm pretty good at it as well. Lately, I've been experimenting with making different mixed drinks. My current favorite is a Daquiri since it is so simple and still manages to be so delicious.  

I maintain a number of different computer systems. Some are for my research but most are so that I can be somewhat technologically self sufficient. I self host a large portion of the online tools that I use day in and day out.

I enjoy camping in Hoosier National forest a ton. I've gone camping in New Mexico and Colorado as well. Backpacking is a blast and hiking in general is also quite fun. 

I write about the things I find interesting on this website in the form of short blog posts.
In high school, I always wanted to write short stories so I still do that occasionally and I may post some of that content here mostly for my own purposes. I doubt many people want to read that. 
