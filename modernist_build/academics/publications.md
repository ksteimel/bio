---

title: Publications
layout: default

---

- [Part of Speech Tagging in Luyia: A Bantu Macrolanguage]({{site.baseurl}}/assets/documents/coling2018_Steimel.pdf)
- [Towards Determining Textual Characteristics of High and Low Impact Publications](http://lrec-conf.org/workshops/lrec2018/W6/pdf/2_W6.pdf)
- [IUCL at SemEval2016 Task 6: An Ensemble Model for Stance Detection in Twitter](http://www.aclweb.org/anthology/S16-1064)
- [Preliminary results from the Free Linguistic Environment project](http://web.stanford.edu/group/cslipublications/cslipublications/HPSG/2016/headlex2016-cmhs.pdf) 
