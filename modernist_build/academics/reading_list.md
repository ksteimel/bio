---

title: Reading List
layout: default

---

- [ ] Machine Learning Evaluation: A Classification Perspective, Nathalie Japkowicz
- [X] On clitics, Arnold Zwicky 1977
- [ ] [What's in a Domain? Learning Domain-Robust Text Representations using Adversarial Training](http://aclweb.org/anthology/N18-2076), Li, Baldwin & Cohn 2018
- [ ] [Continuous Representation of Location for Geolocation and Lexical Dialectology using Mixture Density Networks](http://aclweb.org/anthology/D17-1016)
- [ ] [A Computational Model for the Linguistic Notion of Morphological Paradigm](http://coling2018.org/wp-content/uploads/2018/08/coling18-main.pdf), Silfberg
- [ ] Hard Times, Charles Dickens
- [X] A General-Purpose Tagger with Convolutional Neural Networks, Yu Falenska & Vu
