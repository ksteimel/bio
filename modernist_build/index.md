---
layout: default 
---
<div class="row">
  <div class="column">
I am a Computational Linguistics student at [Indiana University](http://cl.indiana.edu/). 

- [More information about me](bio.html)
- [My current CV]({{site.baseurl}}/assets/documents/curriculum-vitae.pdf)
- [List of publications](academics/publications.html)
- [Subscribe via RSS](feed.xml)
- [Resources for IU Students](resources.html) 
- [I maintain a wiki with some useful information](https://wiki.ksteimel.duckdns.org)
- [Reading list](academics/reading_list.html)

</div>
<div class="column">
![Picture of Kenneth](assets/images/NewSelfie.jpg)
</div>
</div>

---

## Intrests:

[High Performance Computing](https://hpnlp.org)

[Natural Language Processing](projects.html)

[Coffee Roasting](coffee.html)

[Homelabbing](machines.html)

## Blog posts:
{% for post in site.posts %}
   ___
   {% include post_block.html %}
   ___
{% endfor %}
